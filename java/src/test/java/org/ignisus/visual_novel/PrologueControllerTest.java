package org.ignisus.visual_novel;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;

@ExtendWith(ApplicationExtension.class)
public class PrologueControllerTest {

    private PrologueController controller;

    @BeforeEach
    void setUp() {
        controller = new PrologueController();
        controller.initialize();
    }

    @Test
    void testInitialize() {
        assertNotNull(controller.dialogue, "dialogue error");
        assertNotNull(controller.background, "background error");
    }
}
