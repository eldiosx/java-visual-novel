package org.ignisus.visual_novel;

//Java
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

//JavaFX
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Popup;
import javafx.stage.Screen;

//MySql
import java.sql.SQLException;

//Local
import org.ignisus.visual_novel.controllers.SceneController;
import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.Voices;

public class App extends Application {

	ClassLoader classLoader = getClass().getClassLoader();
	// Responsive
	double screenWidth = Screen.getPrimary().getVisualBounds().getWidth();
	double screenHeight = Screen.getPrimary().getVisualBounds().getHeight();
	private SceneController main = new SceneController();
	private Stage stage = new Stage();
	double responsive = screenWidth * 0.07;
	private Font titleFont;
	// Sound
	private SFX sfx = new SFX();
	private Voices voices = new Voices();
	private Environment environment = new Environment();
	private Loader loader;
	// Media
	private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
	private static final String ICON_URL = "file:" + RESOURCES_PATH + "/icons/icon.png";
	private static final String BACKGROUND_URL = "file:" + RESOURCES_PATH + "/images/darkforest.gif";
	private static final String NEW_GAME_BUTTON_URL = "file:" + RESOURCES_PATH + "/icons/off/NewGameButton.png";
	private static final String LOAD_GAME_BUTTON_URL = "file:" + RESOURCES_PATH + "/icons/off/LoadButton.png";
	private static final String SETTINGS_BUTTON_URL = "file:" + RESOURCES_PATH + "/icons/off/SettingsButton.png";
	private static final String EXIT_BUTTON_URL = "file:" + RESOURCES_PATH + "/icons/off/QuitButton.png";
	private static final String CLOSE_BUTTON_URL = "file:" + RESOURCES_PATH + "/icons/off/X.png";
	Image icon = new Image(ICON_URL);

	@Override
	public void start(Stage primaryStage) {
		// Loader
		loader = new Loader();
		loader.setTitle("Loading...");
		loader.getIcons().add(icon);
		loader.show();

		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				for (int i = 0; i < 100; i++) {
					updateProgress(i, 99);
					Thread.sleep(50);
				}
				return null;
			}
		};

		task.setOnSucceeded(event -> {
			loader.hide();
			environment.playAudio(RESOURCES_PATH + "/audio/lullabyPiano.ogg");
			primaryStage.setFullScreen(true);
			primaryStage.show();

		});

		loader.getProgressIndicator().progressProperty().bind(task.progressProperty());
		new Thread(task).start();

		// Load img (1true suavize 2true responsive)
		Image newGameButtonImage = new Image(NEW_GAME_BUTTON_URL, responsive, responsive / 3, true, true);
		Image loadGameButtonImage = new Image(LOAD_GAME_BUTTON_URL, responsive, responsive / 3, true, true);
		Image settingsButtonImage = new Image(SETTINGS_BUTTON_URL, responsive, responsive / 3, true, true);
		Image exitButtonImage = new Image(EXIT_BUTTON_URL, responsive, responsive / 3, true, true);
		Image closeButtonImage = new Image(CLOSE_BUTTON_URL, responsive / 3, responsive / 3, true, true);

		// Button container
		HBox topButtonsBox = new HBox();
		topButtonsBox.setAlignment(Pos.CENTER);
		topButtonsBox.setSpacing(20);
		topButtonsBox.setPadding(new Insets(0, 50, 0, 50));
		ImageView closeButtonImageView = new ImageView(closeButtonImage);
		closeButtonImageView.setFitWidth(60);
		closeButtonImageView.setFitHeight(60);
		ImageView newGameButtonImageView = new ImageView(newGameButtonImage);
		newGameButtonImageView.setPreserveRatio(true);
		newGameButtonImageView.setSmooth(true);
		newGameButtonImageView.setFitHeight(responsive / 1.5);
		newGameButtonImageView.setOnMouseClicked(event -> {
			sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
			environment.stopAudio();
			try {
				main.start(stage);
				primaryStage.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			// // Abrir otra escena .XFML
			// FXMLLoader loader = new
			// FXMLLoader(getClass().getResource("Prologue/Prologue1.fxml"));
			// Parent root = null;
			// try {
			// root = loader.load();
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// Scene scene = new Scene(root);
			// Stage newStage = new Stage();
			// newStage.setScene(scene);
			// newStage.show();
		});
		topButtonsBox.getChildren().add(newGameButtonImageView);

		ImageView loadGameButtonImageView = new ImageView(loadGameButtonImage);
		loadGameButtonImageView.setPreserveRatio(true);
		loadGameButtonImageView.setSmooth(true);
		loadGameButtonImageView.setFitHeight(responsive / 1.5);
		loadGameButtonImageView.setOnMouseClicked(event -> {
			sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
			loadGameButtonImageView.setDisable(true);
			Popup loadPopup = new Popup();
			closeButtonImageView.setOnMouseClicked(e -> {
				sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
				loadPopup.hide();
			});
			loadPopup.setOnHidden(e -> loadGameButtonImageView.setDisable(false));
			VBox loadLayout = new VBox();
			loadLayout.setOpacity(0.65);
			loadLayout.setStyle("-fx-background-color: #111111;");
			loadLayout.setPrefHeight(500);
			loadLayout.setPrefWidth(500);

			Label label = new Label(" Load Game ");
			label.setStyle("-fx-font-size: 50px; -fx-text-fill: white;");
			Label slot1Label = new Label("Slot 1: ");
			Button slot1Button = new Button("0/00/0000 - Chapter 1");
			slot1Label.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");

			slot1Button.setOnAction(event2 -> {
				sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
				environment.stopAudio();
				goToNextStage("/fxml/ep1.fxml", "Prologue");
			});

			Label slot2Label = new Label("Slot 2: ");
			Button slot2Button = new Button("Empty save...");
			slot2Label.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");

			slot2Button.setOnAction(event2 -> {
			});

			Label slot3Label = new Label("Slot 3: ");
			Button slot3Button = new Button("Empty save...");
			slot3Label.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");
			HBox saveData1 = new HBox(slot1Label, slot1Button);
			saveData1.setStyle("-fx-padding: 20px;");
			HBox saveData2 = new HBox(slot2Label, slot2Button);
			saveData2.setStyle("-fx-padding: 20px;");
			HBox saveData3 = new HBox(slot3Label, slot3Button);
			saveData3.setStyle("-fx-padding: 20px;");
			HBox header = new HBox(closeButtonImageView, label);
			header.setStyle("-fx-background-color: #111111; -fx-padding: 20px;");
			header.setPrefHeight(50);
			header.setSpacing(5);
			loadLayout.getChildren().addAll(header, saveData1, saveData2, saveData3);
			Scene loadScene = new Scene(loadLayout, 400, 400);
			Group root = new Group();
			root.getChildren().add(loadScene.getRoot());
			DropShadow dropShadow = new DropShadow();
			dropShadow.setColor(Color.BLACK);
			dropShadow.setRadius(55);
			root.setEffect(dropShadow);
			loadPopup.getContent().add(root);
			loadPopup.show(primaryStage);
		});
		topButtonsBox.getChildren().add(loadGameButtonImageView);

		VBox bottomButtonsBox = new VBox();
		bottomButtonsBox.setAlignment(Pos.CENTER);
		bottomButtonsBox.setSpacing(20);
		bottomButtonsBox.setPadding(new Insets(0, 50, 0, 50)); // Agregar un padding

		ImageView settingsButtonImageView = new ImageView(settingsButtonImage);
		settingsButtonImageView.setPreserveRatio(true);// Mentiene la relacion de aspecto
		settingsButtonImageView.setSmooth(true); // Suvizar el escalado
		settingsButtonImageView.setFitHeight(responsive / 1.5); // Escalado responsive
		settingsButtonImageView.setOnMouseClicked(event -> {
			sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
			settingsButtonImageView.setDisable(true);
			Popup settingsPopup = new Popup();
			closeButtonImageView.setOnMouseClicked(e -> {
				sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
				settingsPopup.hide();

			});

			// Reactivar el boton de settings
			settingsPopup.setOnHidden(e -> settingsButtonImageView.setDisable(false));

			// Crear una nueva escena para la ventana de settings
			VBox settingsLayout = new VBox();
			settingsLayout.setOpacity(0.65); // configurar la opacidad a 0.75
			settingsLayout.setStyle("-fx-background-color: #111111; -fx-padding: 20px;");
			settingsLayout.setPrefHeight(500);
			settingsLayout.setPrefWidth(500);
			Label label = new Label(" Settings ");
			label.setStyle("-fx-font-size: 50px; -fx-text-fill: white;");
			Label musicLabel = new Label("Música:");
			musicLabel.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");
			Slider musicSlider = new Slider(0, 100, 50); // creación del Slider
			musicSlider.valueProperty().addListener(new ChangeListener<Number>() {
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					environment.setVolume((float) musicSlider.getValue() / 100);
				}
			});
			Label soundLabel = new Label("SFX:");
			soundLabel.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");
			Slider soundSlider = new Slider(0, 100, 50);
			soundSlider.valueProperty().addListener(new ChangeListener<Number>() {
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					sfx.setVolume((float) soundSlider.getValue() / 100);
				}
			});
			Label voiceVLabel = new Label("Voces:");
			voiceVLabel.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");
			Slider voiceVSlider = new Slider(0, 100, 50);
			voiceVSlider.valueProperty().addListener(new ChangeListener<Number>() {
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					voices.setVolume((float) voiceVSlider.getValue() / 100);
				}
			});
			Label languageLabel = new Label("Text language:");
			// COnfiguraciones del idioma mediante listas:
			languageLabel.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");
			ObservableList<String> listLanguage = FXCollections.observableArrayList("Español", "English", "Francaise");
			ComboBox<String> languageBox = new ComboBox<>(listLanguage);
			Label voiceLabel = new Label("Voice language:");
			voiceLabel.setStyle("-fx-font-size: 25px; -fx-text-fill: white;");
			ObservableList<String> listVoice = FXCollections.observableArrayList("Español");
			ComboBox<String> languageVoiceBox = new ComboBox<>(listVoice);

			// Agregar los controles a la escena de settings
			HBox header = new HBox(closeButtonImageView, label);
			header.setStyle("-fx-background-color: #111111; -fx-padding: 20px;");
			header.setPrefHeight(50);
			header.setSpacing(5);
			settingsLayout.getChildren().addAll(header, musicLabel, musicSlider, soundLabel, soundSlider, voiceVLabel,
					voiceVSlider, languageLabel, languageBox, voiceLabel, languageVoiceBox);
			Scene settingsScene = new Scene(settingsLayout, 400, 400);
			Group root = new Group();
			root.getChildren().add(settingsScene.getRoot());
			DropShadow dropShadow = new DropShadow();
			dropShadow.setColor(Color.BLACK);
			dropShadow.setRadius(55);
			root.setEffect(dropShadow);
			settingsPopup.getContent().add(root);
			settingsPopup.show(primaryStage);
		});
		bottomButtonsBox.getChildren().add(settingsButtonImageView);

		ImageView exitButtonImageView = new ImageView(exitButtonImage);
		exitButtonImageView.setPreserveRatio(true);// Mentiene la relacion de aspecto
		exitButtonImageView.setSmooth(true); // Suvizar el escalado
		exitButtonImageView.setFitHeight(responsive / 1.5); // Escalado responsive
		exitButtonImageView.setOnMouseClicked(event -> {
			sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
			primaryStage.close();
			Platform.exit();
			System.exit(0);
		});
		bottomButtonsBox.getChildren().add(exitButtonImageView);

		VBox buttonsBox = new VBox();
		buttonsBox.setAlignment(Pos.BOTTOM_CENTER);
		buttonsBox.setSpacing(20);
		buttonsBox.getChildren().addAll(topButtonsBox, bottomButtonsBox);
		buttonsBox.setPadding(new Insets(0, 0, responsive / 2, 0));
		// Title
		try {
			titleFont = Font.loadFont(new FileInputStream(new File(RESOURCES_PATH + "/fonts/PaintDropsRegular.ttf")),
					12);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Label titleLabel = new Label("Your Favorite B-Movie");
		Label subtitleLabel = new Label("Video Game");
		titleLabel.setStyle("-fx-font-size: " + responsive + "px; -fx-text-fill: #030304; -fx-font-family: '"
				+ titleFont.getName() + "';");
		subtitleLabel.setStyle("-fx-font-size: " + responsive + "px; -fx-text-fill: #030304; -fx-font-family: '"
				+ titleFont.getName() + "';");
		DropShadow dropShadow = new DropShadow();
		dropShadow.setColor(Color.WHITE);
		dropShadow.setRadius(10);
		dropShadow.setOffsetX(4);
		dropShadow.setOffsetY(4);
		titleLabel.setEffect(dropShadow);
		subtitleLabel.setEffect(dropShadow);
		titleLabel.setPadding(new Insets(30, 50, 0, 50));
		VBox title = new VBox();
		title.getChildren().addAll(titleLabel, subtitleLabel);
		title.setStyle("-fx-alignment: center; -fx-padding: " + responsive * 1.2 + "px;");

		// Crear el contenedor principal
		BorderPane root = new BorderPane();
		root.setBackground(new Background(new BackgroundImage(new Image(BACKGROUND_URL), BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
				new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, true, true))));
		// Titule
		root.setTop(title);
		BorderPane.setAlignment(title, Pos.CENTER);
		root.setCenter(buttonsBox);

		// Crear la escena
		Scene scene = new Scene(root, screenWidth, screenHeight);
		scene.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("Your Favorite B-Movie Video Game");
		primaryStage.getIcons().add(icon);
		primaryStage.setResizable(false);

		// ButtonOn
		setButtonBehavior(newGameButtonImageView, "NewGameButton.png");
		setButtonBehavior(loadGameButtonImageView, "LoadButton.png");
		setButtonBehavior(settingsButtonImageView, "SettingsButton.png");
		setButtonBehavior(exitButtonImageView, "QuitButton.png");
		setButtonBehavior(closeButtonImageView, "X.png");

	}

	private void setButtonBehavior(ImageView buttonImageView, String imagePath) {
		buttonImageView.setOnMouseEntered(event -> {
			Image newImage = new Image(new File(RESOURCES_PATH + "/icons/on/" + imagePath).toURI().toString(),
					responsive, responsive / 3, true, true);
			buttonImageView.setImage(newImage);
			sfx.playAudioAsync(RESOURCES_PATH + "/audio/select.au");
		});

		buttonImageView.setOnMouseExited(event -> {
			buttonImageView.setImage(new Image(new File(RESOURCES_PATH + "/icons/off/" + imagePath).toURI().toString(),
					responsive, responsive / 3, true, true));
		});
	}

	private void goToNextStage(String fxmlPath, String stageTitle) {
		try {
			voices.stopAudio();
			environment.stopAudio();
			Stage stage = SceneController.createStage(fxmlPath, stageTitle);
			SceneController.showStage(stage);
			SceneController.hideStage(SceneController.getPrimaryStage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws SQLException {
		launch(args);
		Platform.exit();
		System.exit(0);
	}

}