package org.ignisus.visual_novel.controllers;

import java.io.File;

import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.Voices;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Ep1Chang {// Chang
	private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
	// Sound
	private Voices voices = new Voices();
	private Environment environment = new Environment();
	private SFX sfx = new SFX();
	private int currentIndex = 0;
	private String chang1 = "Todo empezó una tarde de agosto, hace un par de años, yo acababa de cumplir catorce años y estaba de picnic con los amigos de mi pueblo de la sierra. Éramos cinco o así, y a eso de las ocho y media de la tarde, cuando empieza a oscurecer, a uno de mis compañeros se le ocurrió la idea de hacer una ouija.\r\n"
			+ "Yo me quería largar de allí cuanto antes y me fui con otro de los colegas un poco más lejos de dónde estaban ellos con la ouija. Al volver los dos hacia el lugar donde se produjo la supuesta invocación, el resto de mis compañeros nos advirtieron que habían contactado con un espíritu de una mujer, que les había dicho que tendríamos problemas para volver de vuelta a casa.\r\n"
			+ "Dos de mis amigos bajaron en la moto y avisaron a mi primo para que viniera a recogernos al resto. Cuando vino mi primo con su coche nos montamos y prácticamente nada más arrancar, el motor se paró. Los que íbamos con él, nos miramos, mi primo volvió a arrancar el coche… y se para de nuevo… y así hasta cuatro veces. Conseguimos arrancar y salir de allí con muy mal cuerpo, y cuando íbamos camino al pueblo notamos que ganábamos velocidad con demasiada facilidad y de repente el coche se frenó de nuevo en seco y quedó atravesado en la carretera.\r\n";
	private String travis1 = "Eso es mentira seguro";
	private String chang2 = "No lo es, te lo juro, estaba allí y aquello dio muy mal rollo\r\n";
	private String travis2 = "Espera, he visto una sombra";
	private String chan03 = "Esa puerta no deberia estar abierta...";
	private Timeline timeline;

	@FXML
	private Label dialogue;
	@FXML
	private Button myButton;

	@FXML
	public void initialize() {
		environment.playAudio(RESOURCES_PATH + "/audio/firecamp.ogg");
		voices.playAudio(RESOURCES_PATH + "/audio/chan01.ogg");
		myButton.setOnAction(event -> {
			goToNextStage("/fxml/Stage62.fxml", "Ep1");
		});

		dialogue.setFont(Font.font("Arial", 24));
		timeline = new Timeline(new KeyFrame(Duration.seconds(0.05), event -> {
			if (currentIndex > chang1.length()) {
				timeline.stop();
			} else {
				String currentText = chang1.substring(0, currentIndex);
				dialogue.setText(currentText);
				currentIndex++;
			}
		}));
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.play();

	}

	private void goToNextStage(String fxmlPath, String stageTitle) {
		try {
			voices.stopAudio();
			environment.stopAudio();
			Stage stage = SceneController.createStage(fxmlPath, stageTitle);
			SceneController.showStage(stage);
			SceneController.hideStage(SceneController.getPrimaryStage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
