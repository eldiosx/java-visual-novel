package org.ignisus.visual_novel.controllers;

import java.io.File;

import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Voices;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Ep1Jhon {
    private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
    // Sound
    private Voices voices = new Voices();
    private Environment environment = new Environment();
    private int currentIndex = 0;
    private String jhon1 = "Vale, pues os voy a contar mi historia: Había un anciano señor en una vieja casa no muy lejos de aquí. El reloj de cucú de aquel hombre marcó las 10:00 de la noche, mientras este apagaba las lámparas de aceite alrededor de su casa para ir a dormir, había sido un día muy pesado. Poco después de que el reloj diera las 10:00 de la noche, cuando ya había ido a dormir, se escuchó tocar la puerta.- TOC, TOC, TOC - Algo extrañado, encendió la lámpara de la mesa de noche, y fue destinado a abrir la puerta. Y al abrirla, alcanzó a ver por el brillo que emanaba su lámpara, a una niña de no más de 7 años, totalmente quemada.";
    private String marlene = "Que horroooor!";
    private String jhon2 = "Voy a seguir: El viejo señor cerró la puerta de un golpe, cerró las ventanas y se acostó a dormir. Al día siguiente, exactamente a la misma hora sucedió lo mismo cuando ya se estaba acomodando para dormir.\r\n"
            + "Fue por eso que al día siguiente se lo comentó, a un amigo que vivía cerca de su casa, y este le recomendó ir a una señora espiritista que vivía a las afueras del pueblo. \r\n"
            + "Salió tempranito, cuando a lo lejos divisó el tarantín rojo donde hacía su trabajo aquella vieja bruja. Al entrar, a la señora le dió una fuerte corazonada cuando vio entrar a Demetrio, pero no le hizo caso. -Bienvenido, que desea?- Preguntó Madame lalou a Demetrio. Éste contó lo que le sucedía, y ésta comenzó a llorar. Le dijo que esa niña era su sobrina, que había muerto en un incendio a las 10:08 de la noche, ella estaba dormida, y tenía sed.\r\n"
            + "Toda su familia murió en ese incendio, pero es el alma de mi sobrina María la que vaga, pidiendo un poco de agua. Demetrio aún con los pelos erizados, preguntó:\r\n"
            + "– Y no hay forma alguna de hacer que se vaya?, que no me moleste mas?\r\n"
            + "– Sí, sí hay una forma- Dijo la anciana – Cuando la niña se aparezca de nuevo por su casa, sencillamente ábrale la puerta y dele un poco de agua, es la única forma de que deje de molestarlo.\r\n"
            + "Ese día, Demetrio padeció un escalofrío constante a lo largo de su cuerpo. El reloj, dió las 10. Faltaban 8 minutos!, Demetrio se ponía las manos en la cabeza y sudaba a litros. De repente, se escuchó el crujir de la paja seca, y…… TOC, TOC, TOC.\r\n"
            + "Demetrio brincó, corrió a la cocina, y tomó agua fresca en un tarro que ya tenía preparado para ese momento. Abrió la puerta. Y ahí estaba, aquel cuerpo casi amorfo a causa de las llamas que la cobijaron aquella noche, a aquella misma hora. -Agua!!!, por favor agua!!, AGUA!!-Suplicó la niña.\r\n"
            + "Ya con el tarro en la mano y sin pensarlo dos veces, Demetrio le sirvió agua, la cual la niña bebió en un segundo. -Más por favor!!- Le dijo la niña de nuevo.\r\n"
            + "Este le sirvió de nuevo. En los ojos de la pequeña niña, Demetrio distinguió perfectamente, como si fuera una película, el momento en el que la niña se quemaba, un frío intenso heló la piel de Demetrio, y en los ojos de la infante, aún se reflejaba el momento de su tragedia. \r\n"
            + "Ya acabado el tarro, la niña se volteó y se fue, atravesando los matorrales, atravesando el bosque como si flotara, hasta que se perdió de los ojos de aquel asustado hombre. Se volteó y se acostó, su cama se encontraba hirviendo!!!, como si fuera el infierno propio.";

    private Timeline timeline;

    @FXML
    private Label dialogue;
    @FXML
    private Button continue1;
    @FXML
    private Button continue2;
    @FXML
    private Button continue3;

    @FXML
    public void initialize() {
        continue2.setVisible(false);
        continue3.setVisible(false);
        initializeTimeline();
    }

    private void initializeTimeline() {
        environment.playAudio(RESOURCES_PATH + "/audio/firecamp.ogg");
        voices.playAudio(RESOURCES_PATH + "/audio/jhon03.ogg");
        handleDialogue(jhon1);
        continue1.setOnAction(event -> part2());
    }

    private void part2() {
        timeline.stop();
        continue1.setVisible(false);
        continue2.setVisible(true);
        voices.stopAudio();
        voices.playAudio(RESOURCES_PATH + "/audio/marlene03.ogg");
        handleDialogue(marlene);
        continue2.setOnAction(event -> part3());
    }

    private void part3() {
        timeline.stop();
        continue2.setVisible(false);
        continue3.setVisible(true);
        voices.stopAudio();
        voices.playAudio(RESOURCES_PATH + "/audio/jhon04.ogg");
        handleDialogue(jhon2);
        continue3.setOnAction(event -> {
            goToNextStage("/fxml/ep1.fxml", "Prologue");
        });
    }

    private void handleDialogue(String text) {
        currentIndex = 0;
        dialogue.setText("");
        dialogue.setFont(Font.font("Arial", 24));
        timeline = new Timeline(new KeyFrame(Duration.seconds(0.05), event -> {
            if (currentIndex > text.length()) {
                timeline.stop();
            } else {
                String currentText = text.substring(0, currentIndex);
                dialogue.setText(currentText);
                currentIndex++;
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    private void goToNextStage(String fxmlPath, String stageTitle) {
        try {
            voices.stopAudio();
            environment.stopAudio();
            Stage stage = SceneController.createStage(fxmlPath, stageTitle);
            SceneController.showStage(stage);
            SceneController.hideStage(SceneController.getPrimaryStage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
