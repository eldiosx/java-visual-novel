package org.ignisus.visual_novel.controllers;

import java.io.File;

import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Voices;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Ep1Helen {// Helen
	private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
	// Sound
	private Voices voices = new Voices();
	private Environment environment = new Environment();
	private SFX sfx = new SFX();
	private int currentIndex = 0;
	private String helen01 = "Yo no creo en estas cosas, la verdad, pero he de decir que nos pasó una cosa bastante extraña volviendo de vacaciones a Connecticut cuando nos encontramos  un hombre vestido de blanco haciendo autostop en medio de la carretera. \r\n"
			+ "Mi madre dijo de parar a recogerlo y mi padre siguió conduciendo diciéndole que cómo íbamos a recoger a un extraño. El caso es que mi madre lo consiguió convencer y, paramos un poco después de rebasarle, y al mirar no había nadie. FIN";
	private String jhon05 = "Pues yo creo que era una aparición de alguien que se habría muerto en la carretera y nunca pudo llegar a casa… En fin… Deberíamos ir yéndonos a dormir, que se está haciendo tarde.";
	private Timeline timeline;

	@FXML
	private Label dialogue;
	@FXML
	private Button myButton;
	@FXML
	private Button go;
	@FXML
	private Button stay;

	@FXML
	public void initialize() {
		environment.playAudio(RESOURCES_PATH + "/audio/firecamp.ogg");
		voices.playAudio(RESOURCES_PATH + "/audio/helen01.ogg");
		myButton.setOnAction(event -> {
			goToNextStage("/fxml/Stage52.fxml", "Prologue");
		});

		dialogue.setFont(Font.font("Arial", 24));
		timeline = new Timeline(new KeyFrame(Duration.seconds(0.05), event -> {
			if (currentIndex > helen01.length()) {
				timeline.stop();
			} else {
				String currentText = helen01.substring(0, currentIndex);
				dialogue.setText(currentText);
				currentIndex++;
			}
		}));
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.play();

	}

	private void goToNextStage(String fxmlPath, String stageTitle) {
		try {
			voices.stopAudio();
			environment.stopAudio();
			Stage stage = SceneController.createStage(fxmlPath, stageTitle);
			SceneController.showStage(stage);
			SceneController.hideStage(SceneController.getPrimaryStage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
