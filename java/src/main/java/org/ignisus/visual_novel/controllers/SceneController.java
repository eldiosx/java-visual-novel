package org.ignisus.visual_novel.controllers;

import java.io.File;

import org.ignisus.visual_novel.controllers.SceneController;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class SceneController extends Application {

	private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
	static double screenHeight = Screen.getPrimary().getVisualBounds().getHeight();
	static double screenWidth = Screen.getPrimary().getVisualBounds().getWidth();
	double responsive = screenWidth * 0.07;
	private static Stage prologue;
	private static Stage currentStage;
	static Image icon = new Image(new File(RESOURCES_PATH + "/icons/icon.png").toURI().toString());

	@Override
	public void start(Stage prologue) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/prologue.fxml"));
		SceneController.prologue = prologue;
		SceneController.currentStage = prologue;
		prologue.setTitle("Your Favorite B-Movie");
		prologue.setScene(new Scene(root, screenWidth, screenHeight));
		prologue.getIcons().add(icon);
		prologue.setResizable(false);
		prologue.show();
		prologue.setFullScreen(true);
	}

	public static void main(String[] args) {
		launch(args);
		Platform.exit();
		System.exit(0);
	}

	public static void hideStage(Stage stage) {
		stage.hide();
	}

	public static void showStage(Stage stage) {
		SceneController.currentStage = stage;
		stage.setTitle("Your Favorite B-Movie");
		stage.setResizable(false);
		stage.show();
		stage.setFullScreen(true);
	}

	public static Stage createStage(String fxmlFile, String title) throws Exception {
		Stage stage = new Stage();
		Parent root = FXMLLoader.load(SceneController.class.getResource(fxmlFile));
		stage.setTitle(title);
		stage.setScene(new Scene(root, screenWidth, screenHeight));
		stage.getIcons().add(icon);
		SceneController.currentStage = stage;
		stage.setFullScreen(true);
		return stage;
	}

	public static Stage getPrimaryStage() {
		return prologue;
	}

	public static Stage getCurrentStage() {
		return currentStage;
	}

}
