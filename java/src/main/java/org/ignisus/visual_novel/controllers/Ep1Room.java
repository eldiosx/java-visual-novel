package org.ignisus.visual_novel.controllers;

import java.io.File;
import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Voices;
import org.ignisus.visual_novel.Credits;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class Ep1Room {

    private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();

    // Sound
    private Voices voices = new Voices();
    private Environment environment = new Environment();
    private SFX sfx = new SFX();
    Credits credits = new Credits();

    Stage stage = new Stage();

    @FXML
    private Button body;
    @FXML
    private Button hand;
    @FXML
    private Button continueEnd;
    @FXML
    private Button knife;
    @FXML
    private Label knifeText;
    @FXML
    private Button room;
    @FXML
    private Button blood;
    @FXML
    private Button floor;
    @FXML
    private Label bodytext;
    @FXML
    private Label handtext;
    @FXML
    private Label roomtext;
    @FXML
    private Label bloodtext;
    @FXML
    private Label floortext;

    int count = 0;
    boolean isRunning = true;

    @FXML
    public void initialize() {
        continueEnd.setDisable(true);
        environment.playAudio(RESOURCES_PATH + "/audio/horrorHeartbeat.ogg");

        continueEnd.setOnAction(event -> handleContinueEndButtonClick());

        configureButtonEvent(floor, floortext, 3);
        configureButtonEvent(room, roomtext, 4);
        configureButtonEvent(hand, handtext, 3);
        configureButtonEvent(knife, knifeText, 3);
        configureButtonEvent(blood, bloodtext, 3);
        configureButtonEvent(body, bodytext, 3);
    }

    private void handleContinueEndButtonClick() {
        try {
            sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
            environment.stopAudio();
            credits.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void configureButtonEvent(Button button, Label labelText, int threshold) {
        button.setOnAction(event -> {
            try {
                sfx.playAudioAsync(RESOURCES_PATH + "/audio/rope.au");
                labelText.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if (count >= threshold) {
                disableRemainingButtons();
            }
        });
    }

    private void disableRemainingButtons() {
        for (Node node : body.getParent().getChildrenUnmodifiable()) {
            if (node instanceof Button && !node.isDisabled()) {
                ((Button) node).setDisable(true);
                continueEnd.setDisable(false);
            }
        }
    }
}
