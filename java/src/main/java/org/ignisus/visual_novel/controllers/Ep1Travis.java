package org.ignisus.visual_novel.controllers;

import java.io.File;

import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.Voices;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Ep1Travis {// travis comienzo
	private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
	// Sound
	private Voices voices = new Voices();
	private Environment environment = new Environment();
	private SFX sfx = new SFX();
	private int currentIndex = 0;
	private String travis1 = "Al parecer una noche de 1951, una de las prostitutas que ejercían su labor en un prostíbulo no muy lejos de aquí, apareció muerta en su cama con una puñalada en el vientre. El FBI, después de realizar sus investigaciones concluyeron en que se trataba de algo súper raro.\r\n";
	private String marlene04 = "Lo siento chicos, no me encuentro bien, me voy a ir a la cama";
	private String travis02 = "Descansa, ya habrá más noches para contar historias.";
	private String travis03 = "Bueno, sigo con la historia:  Nadie oyó los gritos de la mujer mientras era asesinada. En sus manos había heridas producidas por la hoja de un cuchillo y todo aparentaba como si hubiera sido ella misma la que se lo hubiera clavado. Los forenses determinaron un suicidio y algo más: la prostituta estaba embarazada. \r\n"
			+ "Dos años después todo parecía haberse olvidado, pero la habitación donde murió la joven no volvió a ser utilizada por ninguna de las otras prostitutas. El dueño del local, mandó cerrar con llave la habitación y nadie entró allí durante varios meses. En verano de 1953, una mujer de unos 30 años llegó al prostíbulo. Como aquella noche, todo estaba lleno y claro, el dueño no tuvo más remedio que abrirle la habitación de la otra prostituta muerta dos años antes…\r\n"
			+ "…Cuando abrieron la puerta… en las paredes alguien había dibujado caras con llorando desfiguradas, también había cruces y animales como lechuzas, gatos y ratones muertos colgando del techo con cuerdas…. El dueño del local, viendo aquello, le dijo a la nueva mujer que durmiera con una compañera y a la mañana siguiente mandó limpiar y  pintar el cuarto. Sin embargo, las caras de lamento volvían a aparecer una, y otra y otra y otra vez en la pared. La voz se empezó a correr por la ciudad y un mal día Carlos tuvo que cerrar su negocio y se marchó de la ciudad.\r\n"
			+ "Desde entonces la casa permaneció en ruinas hasta que fue derribada.\r\n" + "";
	private Timeline timeline;

	@FXML
	private Label dialogue;
	@FXML
	private Button myButton;

	@FXML
	public void initialize() {
		environment.playAudio(RESOURCES_PATH + "/audio/firecamp.ogg");
		voices.playAudio(RESOURCES_PATH + "/audio/travis01.ogg");
		myButton.setOnAction(event -> {
			goToNextStage("/fxml/Stage42.fxml", "Prologue");
		});

		dialogue.setFont(Font.font("Arial", 24));
		timeline = new Timeline(new KeyFrame(Duration.seconds(0.05), event -> {
			if (currentIndex > travis1.length()) {
				timeline.stop();
			} else {
				String currentText = travis1.substring(0, currentIndex);
				dialogue.setText(currentText);
				currentIndex++;
			}
		}));
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.play();

	}

	private void goToNextStage(String fxmlPath, String stageTitle) {
		try {
			voices.stopAudio();
			environment.stopAudio();
			Stage stage = SceneController.createStage(fxmlPath, stageTitle);
			SceneController.showStage(stage);
			SceneController.hideStage(SceneController.getPrimaryStage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
