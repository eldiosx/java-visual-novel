package org.ignisus.visual_novel.controllers;

import java.io.File;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.Voices;

public class Prologue {

    private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
    // Sound
    private Voices voices = new Voices();
    private Environment environment = new Environment();
    private SFX sfx = new SFX();
    private int currentIndex = 0;
    private String staff = "Hola soy Javier. Bienvenido al campamento, The Otter. Espero que estés preparado para dos semanas de diversión. Tus compañeros ya están ubicados en sus cabañas, la tuya es la número 7 puedes ir acomodandote si quieres...";
    private String jhon1 = "Hey, pensé que ya no llegabas. ¿Qué tal el viaje?";
    private String jhon2 = "He hablado con Marleene para hacer una fogata esta noche y contar historias. Nos vemos esta noche";
    private String marlene1 = "¡Hey! Se me ha hecho eterno no verte, y eso que sólo ha pasado un mes desde que nos dieron las vacaciones de verano. ¿cómo estas?";
    private String marlene2 = "¿Descansar? Eso es para los viejos, esta noche hay hoguera y vamos a contar historias, hasta Helen se ha animado a venir. Te veo esta noche entonces";
    private Timeline timeline;

    @FXML
    private AnchorPane background;
    @FXML
    private Label dialogue;
    @FXML
    private Button cabin;
    @FXML
    private Button explore;
    @FXML
    private Button lookRight;
    @FXML
    private Button lookLeft;
    @FXML
    private Button goCamp;
    @FXML
    private Button continueButton;
    @FXML
    private Button good;
    @FXML
    private Button meh;
    @FXML
    private Button well;
    @FXML
    private Button tired;
    @FXML
    private Button ofCourse;
    @FXML
    private Button noLike;
    @FXML
    private Button laziness;

    @FXML
    public void initialize() {
        initializeButtons();
        initializeTimeline();
    }

    private void initializeButtons() {
        hideButtons();
        cabin.setOnAction(event -> handleCabinButton());
        explore.setOnAction(event -> handleExploreButton());
        lookRight.setOnAction(event -> handleLookRightButton());
        lookLeft.setOnAction(event -> handleLookLeftButton());
    }

    private void initializeTimeline() {
        environment.playAudio(RESOURCES_PATH + "/audio/happyForest.ogg");
        voices.playAudio(RESOURCES_PATH + "/audio/staff.ogg");
        handleDialogue(staff);
    }

    private void hideButtons() {
        lookRight.setVisible(false);
        lookLeft.setVisible(false);
        goCamp.setVisible(false);
        continueButton.setVisible(false);
        good.setVisible(false);
        meh.setVisible(false);
        well.setVisible(false);
        tired.setVisible(false);
        ofCourse.setVisible(false);
        noLike.setVisible(false);
        laziness.setVisible(false);
    }

    private void handleCabinButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        timeline.stop();
        explore.setVisible(false);
        goCamp.setVisible(false);
        cabin.setVisible(false);
        good.setVisible(true);
        meh.setVisible(true);
        background.getStyleClass().removeAll("background");
        background.getStyleClass().removeAll("background2");
        background.getStyleClass().add("background3");
        voices.stopAudio();
        voices.playAudio(RESOURCES_PATH + "/audio/jhon01.ogg");
        handleDialogue(jhon1);
        meh.setOnAction(event -> handleContinueButton());
        good.setOnAction(event -> handleContinueButton());
    }

    private void handleExploreButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        voices.stopAudio();
        timeline.stop();
        background.getStyleClass().removeAll("background");
        background.getStyleClass().add("background2");
        cabin.setVisible(false);
        explore.setVisible(false);
        lookRight.setVisible(true);
        lookLeft.setVisible(true);
        currentIndex = 0;
        dialogue.setText("");
        lookRight.setOnAction(event -> handleLookRightExplore());
        lookLeft.setOnAction(event -> handleLookLeftButton());
    }

    private void handleLookRightExplore() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        timeline.stop();
        lookRight.setVisible(false);
        lookLeft.setVisible(false);
        goCamp.setVisible(true);
        goCamp.setOnAction(event -> handleCabinButton());
    }

    private void handleContinueButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        timeline.stop();
        background.getStyleClass().removeAll("background");
        background.getStyleClass().removeAll("background2");
        background.getStyleClass().add("background3");
        good.setVisible(false);
        meh.setVisible(false);
        voices.stopAudio();
        voices.playAudio(RESOURCES_PATH + "/audio/jhon02.ogg");
        handleDialogue(jhon2);
        continueButton.setVisible(true);
        continueButton.setOnAction(event -> {
            voices.stopAudio();
            environment.stopAudio();
            goToNextStage("/fxml/ep1.fxml", "Prologue");

        });
    }

    private void handleDialogue(String text) {
        currentIndex = 0;
        dialogue.setText("");
        dialogue.setFont(Font.font("Arial", 24));
        timeline = new Timeline(new KeyFrame(Duration.seconds(0.05), event -> {
            if (currentIndex > text.length()) {
                timeline.stop();
            } else {
                String currentText = text.substring(0, currentIndex);
                dialogue.setText(currentText);
                currentIndex++;
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    private void goToNextStage(String fxmlPath, String stageTitle) {
        try {
            timeline.stop();
            voices.stopAudio();
            environment.stopAudio();
            Stage stage = SceneController.createStage(fxmlPath, stageTitle);
            SceneController.showStage(stage);
            SceneController.hideStage(SceneController.getPrimaryStage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleLookRightButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        timeline.stop();
        lookRight.setVisible(false);
        lookLeft.setVisible(false);
        goCamp.setVisible(true);
        goCamp.setOnAction(event -> handleCabinButton());
    }

    private void handleLookLeftButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        timeline.stop();
        background.getStyleClass().removeAll("background");
        background.getStyleClass().removeAll("background2");
        background.getStyleClass().add("background4");
        voices.stopAudio();
        environment.stopAudio();
        lookRight.setVisible(false);
        lookLeft.setVisible(false);
        well.setVisible(true);
        tired.setVisible(true);
        environment.playAudio(RESOURCES_PATH + "/audio/nightForest.ogg");
        voices.playAudio(RESOURCES_PATH + "/audio/marlene01.ogg");
        handleDialogue(marlene1);
        well.setOnAction(event -> handleWellButton());
        tired.setOnAction(event -> handleTiredButton());
    }

    private void handleWellButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        well.setVisible(false);
        tired.setVisible(false);
        ofCourse.setVisible(true);
        noLike.setVisible(true);
        laziness.setVisible(true);
        voices.stopAudio();
        environment.stopAudio();
        goToNextStage("/fxml/ep1.fxml", "Prologue");
    }

    private void handleTiredButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        timeline.stop();
        well.setVisible(false);
        tired.setVisible(false);
        ofCourse.setVisible(true);
        noLike.setVisible(true);
        laziness.setVisible(true);
        voices.stopAudio();
        currentIndex = 0;
        dialogue.setText("");
        environment.playAudio(RESOURCES_PATH + "/audio/nightForest.ogg");
        voices.playAudio(RESOURCES_PATH + "/audio/marlene02.ogg");
        handleDialogue(marlene2);
        ofCourse.setOnAction(event -> handleOfCourseButton());
        noLike.setOnAction(event -> handleNoLikeButton());
        laziness.setOnAction(event -> handleLazinessButton());
    }

    private void handleOfCourseButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        voices.stopAudio();
        environment.stopAudio();
        goToNextStage("/fxml/ep1.fxml", "Prologue");
    }

    private void handleNoLikeButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        voices.stopAudio();
        environment.stopAudio();
        goToNextStage("/fxml/ep1.fxml", "Prologue");
    }

    private void handleLazinessButton() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        voices.stopAudio();
        environment.stopAudio();
        goToNextStage("/fxml/ep1.fxml", "Prologue");
    }
}
