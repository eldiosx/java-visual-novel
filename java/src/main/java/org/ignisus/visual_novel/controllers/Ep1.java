package org.ignisus.visual_novel.controllers;

import java.io.File;
import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.Voices;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Ep1 {
    private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();

    // Sound
    private Voices voices = new Voices();
    private Environment environment = new Environment();
    private SFX sfx = new SFX();

    @FXML
    private Button John;
    @FXML
    private Button leaveButton;
    @FXML
    private Button Marleene;
    @FXML
    private Button Helen;
    @FXML
    private Button Chang;
    @FXML
    private Button Travis;

    private int count = 0;

    @FXML
    public void initialize() {
        leaveButton.setDisable(true);

        Button[] buttons = { John, Marleene, Helen, Chang, Travis };

        for (Button button : buttons) {
            button.setOnAction(event -> handleButtonClick(button));
        }

        leaveButton.setOnAction(event -> handleLeaveButtonClick());
    }

    private void handleButtonClick(Button button) {
        button.setDisable(true);
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        voices.stopAudio();
        environment.stopAudio();
        String fxmlPath = "/fxml/ep1" + button.getText() + ".fxml";
        goToNextStage(fxmlPath, "Ep1");
        count++;
        if (count >= 2) {
            disableRemainingButtons();
        }
    }

    private void handleLeaveButtonClick() {
        sfx.playAudioAsync(RESOURCES_PATH + "/audio/click.au");
        voices.stopAudio();
        environment.stopAudio();
        goToNextStage("/fxml/ep1Room.fxml", "Ep1");
    }

    private void goToNextStage(String fxmlPath, String stageTitle) {
        try {
            voices.stopAudio();
            environment.stopAudio();
            Stage stage = SceneController.createStage(fxmlPath, stageTitle);
            SceneController.showStage(stage);
            SceneController.hideStage(SceneController.getPrimaryStage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void disableRemainingButtons() {
        for (Button button : new Button[] { John, Marleene, Helen, Chang, Travis }) {
            if (!button.isDisabled()) {
                button.setDisable(true);
                leaveButton.setDisable(false);
            }
        }
    }
}
