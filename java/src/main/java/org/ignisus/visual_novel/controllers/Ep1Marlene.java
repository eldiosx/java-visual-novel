package org.ignisus.visual_novel.controllers;

import java.io.File;

import org.ignisus.visual_novel.sound.Environment;
import org.ignisus.visual_novel.sound.Main;
import org.ignisus.visual_novel.sound.SFX;
import org.ignisus.visual_novel.sound.Voices;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Ep1Marlene {// Marlene comienzo
	private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();
	// Sound
	private Voices voices = new Voices();
	private Environment environment = new Environment();
	private SFX sfx = new SFX();
	private int currentIndex = 0;
	private String marlene05 = "La historia que voy a contar le pasó a mi padre cuando era joven, o eso me ha contado. Tenía 19 años y vivía en Francia, en una casa bastante grande que estaba a las afueras.\r\n"
			+ "Mi padre tiene dos hermanas más pequeñas y la más pequeña de los tres tenía 6 años y solía subir a jugar a la buhardilla con las muñecas.\r\n"
			+ "Un día empezó a decirle a mi padre que cuando subía a jugar siempre oía ruidos como si tirasen como una pelota y rodase por el suelo, pero que luego nunca veía nada. Mi padre le quitaba importancia puesto que pensaba que eran las típicas imaginaciones de una niña, pero mi tía insistía en que se oían ruidos sin verse nada.\r\n"
			+ "Mi padre se lo contó a mi abuelo y viendo que mi tía dejó de subir allí a jugar decidieron comprobar un día si era cierto que se oían esos ruidos extraños. Subieron arriba, se colocaron uno a cada lado de la buhardilla con una linterna cada uno y todo apagado y si oían algo encender las linternas para ver si verdaderamente había algo allí que provocase los ruidos.\r\n"
			+ "Permanecieron un buen rato allí parados esperando, y de repente, verdaderamente se empezaron a oír ruidos como los que había mencionado mi tía. Un sonido como si tirasen una nuez desde arriba del todo y cayera rodando por el suelo, y en ese momento encendieron las linternas pero no consiguieron ver nada, de manera que las apagaron y esperaron otra vez pensando que habían llegado tarde a encenderlas y que sería alguna rata o algun animal que se habría colado.\r\n"
			+ ".";
	private String marlene06 = "Estuvieron así un buen rato sin conseguir ver nada, pero oyendo los ruidos que cada vez eran más frecuentes, y al final desistieron y bajaron. La historia quedó ahí y pasaron varios días, uno de ellos le dijo mi abuelo a mi padre que tenía que bajar al pozo a por agua porque no había.\r\n"
			+ "Estaba bastante oscuro y mi padre un poco asustado me dijo que bajó de todos modos. Así que bajó, y empezó a subirle cubos de agua a mi abuelo, hasta que sintió cómo una presencia detrás de él le respiraba cerca. Mi padre se giró rápidamente para ver si veía algo y oyó cómo unas voces suplicaban que se fueran de allí, que no pretendían hacerles daño, pero que ésa era su casa y habían muerto allí y no se iban a ir. Mi padre empezó a gritarle a mi abuelo que por favor le subiese mientras comenzó a ver dos personas que se aparecían mientras le suplicaban que se alejaran de allí.\r\n"
			+ "En cuanto ahorraron un poco se marcharon y la dejaron vacía como ellos pidieron. Mi padre me contó esta historia hace ya muchos años, yo al principio no creía que estas cosas pudieran ser ciertas, pero creo que mi padre no me mentiría en algo así… la verdad…\r\n"
			+ ".";
	private Timeline timeline;

	@FXML
	private Label dialogue;
	@FXML
	private Button myButton;

	@FXML
	public void initialize() {
		environment.playAudio(RESOURCES_PATH + "/audio/firecamp.ogg");
		voices.playAudio(RESOURCES_PATH + "/audio/marlene05.ogg");
		myButton.setOnAction(event -> {
			try {
				// Crear un nuevo Stage
				voices.stopAudio();
				environment.stopAudio();
				goToNextStage("/fxml/Stage32.fxml", "Prologue");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		dialogue.setFont(Font.font("Arial", 24));
		timeline = new Timeline(new KeyFrame(Duration.seconds(0.05), event -> {
			if (currentIndex > marlene05.length()) {
				timeline.stop();
			} else {
				String currentText = marlene05.substring(0, currentIndex);
				dialogue.setText(currentText);
				currentIndex++;
			}
		}));
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.play();

	}

	private void goToNextStage(String fxmlPath, String stageTitle) {
		try {
			voices.stopAudio();
			environment.stopAudio();
			Stage stage = SceneController.createStage(fxmlPath, stageTitle);
			SceneController.showStage(stage);
			SceneController.hideStage(SceneController.getPrimaryStage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
