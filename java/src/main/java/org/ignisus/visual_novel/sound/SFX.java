package org.ignisus.visual_novel.sound;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class SFX {

    private float volume = 0.9f;

    public void playAudioAsync(String filePath) {
        Thread audioThread = new Thread(() -> {
            try {
                playAudio(filePath);
            } catch (UnsupportedAudioFileException | IOException | LineUnavailableException | InterruptedException e) {
                e.printStackTrace();
            }
        });
        audioThread.start();
    }

    private void playAudio(String filePath) throws UnsupportedAudioFileException, IOException, LineUnavailableException, InterruptedException {
        File audioFile = new File(filePath);

        AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
        AudioFormat format = audioStream.getFormat();

        DataLine.Info info = new DataLine.Info(Clip.class, format);
        Clip audioClip = (Clip) AudioSystem.getLine(info);

        audioClip.open(audioStream);
        setVolume(audioClip, volume);
        audioClip.start();

        while (!audioClip.isRunning()) {
            Thread.sleep(10);
        }

        while (audioClip.isRunning()) {
            Thread.sleep(10);
        }

        audioClip.close();
        audioStream.close();
    }

    public void setVolume(Clip clip, float volume) {
        if (clip.isControlSupported(FloatControl.Type.MASTER_GAIN)) {
            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            float range = gainControl.getMaximum() - gainControl.getMinimum();
            float gain = (range * volume) + gainControl.getMinimum();
            gainControl.setValue(gain);
        }
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }
}

