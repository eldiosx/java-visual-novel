package org.ignisus.visual_novel;

//Java
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
//JavaFX
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
//Local
import org.ignisus.visual_novel.sound.Environment;

public class Credits extends Application {
	private Environment environment = new Environment();
	static double screenHeight = Screen.getPrimary().getVisualBounds().getHeight();
	static double screenWidth = Screen.getPrimary().getVisualBounds().getWidth();
	double responsive = screenWidth * 0.07;
	int wait = 60000;
	private static final String RESOURCES_PATH = new File("src/main/resources").getAbsolutePath();

	@Override
	public void start(Stage stage) {
		// Icon
		Image icon = new Image(new File(RESOURCES_PATH + "/icons/icon.png").toURI().toString());
		// Audio de fondo
		environment.playAudio(RESOURCES_PATH + "/audio/horrorBellaciao.ogg");

		// Programar una tarea que se ejecute después de 30 segundos
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				Platform.runLater(() -> {
					// Cerrar la aplicación
					Platform.exit();
					System.exit(0);
				});
			}
		}, wait); // 60 segundos en milisegundos (1min) + lo que surja

		Text creditsText = new Text("Hace muchos tiempo, en un MEDAC casi en Córdoba, un equipo de desarrolladores\n\n"
				+ "empezaron un ambicioso proyecto.\n\n\n\n\n" + "Los disturbios rodean el planeta\n\n"
				+ "Se esta planteando el cobro de impuestos al consumo de oxigeno\n\n"
				+ "y emigrar a sistemas solares con mejores prestaciones.\n\n" + "\n\n"
				+ "Esperando resolver el problema con un bloqueo \n\n" + "táctico a Sanchez, el codicioso, \n\n"
				+ "la alianza rebelde se enfrenta a nuevos retos. \n\n\n\n\n\n"
				+ "Powered by ignisus.org \n\n"
				+ "Developers: \n\n"
				+ "Daniel Delgado \n Aida Fernández \n Adri Márquez\n\n\n\n\n\n"
				+ "Proyecto realizado usando:\n\n" + "Java 21 + FX, jdx, jorbis (ogg), Sql y sql connector\n\n"
				+ "Soporte opus\n\n" + "GLUON Scene Builder\n\n" + "DB Browser\n\n" + "Audacity\n\n" + "Gimp\n\n"
				+ "Linux 6.1 (Workstation & Server)\n\n" + "XAMPP\n\n" + "Oracle Database & Workbench\n\n" + "\n\n"
				+ "\n\n" + "Desarrollo: \n\n" + "Database Manager: Daniel Delgado\n\n" + "Diseño: Aida Fernández\n\n"
				+ "Diseño: Adrián Marquez\n\n" + "Backend & Frontend: Daniel Delgado\n\n" + "\n\n"
				+ "\n\n" + "Actores de voz: \n\n" + "John: Nico Cano \n\n" + "Marleene: Esther Argüelles\n\n"
				+ "Travis: Javier Naranjo\n\n" + "Helen: Aida Fernández\n\n" + "Chan: Adri Marquez \n\n"
				+ "Javier: Daniel Delgado \n\n" + "\n\n" + "\n\n" + "Agradecimientos especiales \n\n"
				+ "Javier naranjo y Esther Argüelles por trabajar a cambio de un vinito \n\n"
				+ "Juan García, Antonio Ruiz, Oscar con K por sus consejillos <3\n\n" + "\n\n" + "\n\n" + "\n\n "
				+ "Repositorio de Gitlab: \n\n" + "https://gitlab.com/eldiosx/java-visual-novel \n\n\n\n\n\n"
				+ "FIN :D\n\n\n\n");
		creditsText.setFont(new Font("Arial", responsive / 3));
		creditsText.setFill(Color.WHITE);
		creditsText.setStyle("-fx-text-alignment: center;");
		creditsText.setY(1000);
		double textWidth = creditsText.getLayoutBounds().getWidth();
		double textX = (screenWidth - textWidth) / 2;
		creditsText.setX(textX);
		Group creditsGroup = new Group(creditsText);

		// Text Animation
		TranslateTransition creditsAnimation = new TranslateTransition();
		creditsAnimation.setDuration(Duration.seconds(55));
		creditsAnimation.setNode(creditsGroup);
		creditsAnimation.setToY(-10000);
		Scene scene = new Scene(creditsGroup, 1000, 1000, Color.BLACK);
		scene.setOnMouseClicked(event -> {
			wait = wait + 5000;
			creditsAnimation.stop();
			creditsAnimation.setToY(-10000);
			creditsAnimation.play();
		});
		// Mostrar la escena
		stage.setScene(scene);
		stage.setTitle("Your Favorite B-Movie");
		stage.setResizable(false);
		stage.getIcons().add(icon);
		stage.show();
		stage.setFullScreen(true);
		creditsAnimation.play();
	}

	public static void main(String[] args) {
		launch();
		Platform.exit();
		System.exit(0);
	}

}
