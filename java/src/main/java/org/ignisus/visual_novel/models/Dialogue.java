package org.ignisus.visual_novel.models;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dialogue {
    private Map<String, List<String>> dialogues;

    public Dialogue(String filePath) {
        this.dialogues = new HashMap<>();
        loadDialogues(filePath);
    }

    private void loadDialogues(String filePath) {
        try {
            String content = new String(Files.readAllBytes(Paths.get(filePath)));
            JSONObject obj = new JSONObject(content);
            JSONArray dialogueArray = obj.getJSONArray("dialogues");

            for (int i = 0; i < dialogueArray.length(); i++) {
                JSONObject dialogue = dialogueArray.getJSONObject(i);
                String id = dialogue.getString("id");
                JSONArray lines = dialogue.getJSONArray("lines");
                List<String> lineList = new ArrayList<>();
                for (int j = 0; j < lines.length(); j++) {
                    lineList.add(lines.getString(j));
                }
                dialogues.put(id, lineList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getDialogueLines(String id) {
        return dialogues.getOrDefault(id, new ArrayList<>());
    }
}
