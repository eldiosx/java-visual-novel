package org.ignisus.visual_novel.utils;

public class SystemInfo {

    /**
     * @return OS name.
     */
    public static String getOSName() {
        return System.getProperty("os.name");
    }

    /**
     * @return OS version.
     */
    public static String getOSVersion() {
        return System.getProperty("os.version");
    }

    /**
     * @return OS architecture.
     */
    public static String getOSArch() {
        return System.getProperty("os.arch");
    }

    /**
     * @return JVM version.
     */
    public static String getJavaVersion() {
        return System.getProperty("java.version");
    }

    /**
     * @return JVM vendor.
     */
    public static String getJavaVendor() {
        return System.getProperty("java.vendor");
    }

    /**
     * @return JFX version.
     */
    public static String getJavafxVersion() {
        return System.getProperty("javafx.version");
    }

    public static void main(String[] args) {
        System.out.println("Operating System: " + getOSName());
        System.out.println("OS Version: " + getOSVersion());
        System.out.println("OS Architecture: " + getOSArch());
        System.out.println("Java Version: " + getJavaVersion());
        System.out.println("Java Vendor: " + getJavaVendor());
        System.out.println("JavaFX version: " + getJavafxVersion());
    }
}
