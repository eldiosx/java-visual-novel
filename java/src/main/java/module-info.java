module org.ignisus.visual_novel {
	requires transitive javafx.controls; // UI Controls
	requires transitive javafx.media; // APIs for audio
	requires transitive gdx; // API GDX for mediaplayer .ogg
	requires transitive jorbis; // API for ogg
	requires transitive javafx.fxml; // FXML
	requires transitive javafx.graphics; // CSS
	requires transitive java.sql; // SQL
	requires transitive java.desktop; // AWT and Swing

	opens org.ignisus.visual_novel to javafx.graphics, javafx.fxml;
	opens org.ignisus.visual_novel.controllers to javafx.graphics, javafx.fxml;

	exports org.ignisus.visual_novel to javafx.graphics, javafx.fxml;
	exports org.ignisus.visual_novel.controllers to javafx.graphics, javafx.fxml;
}
